from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect
from django.views import View
from django.http import HttpResponse
from django.db import transaction
from node_0.forms import UserForms


class Setting(View):
    def dispatch(self, *args, **kwargs):
        return super(Setting, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        user = User.objects.get(id=request.user.id)
        return render(request, 'accounts/account.html', {'user': user})

    def post(self, request, *args, **kwargs):
        form = UserForms(request.POST)

        if form.is_valid():
            user = User.objects.filter(id=request.user.id)

            with transaction.atomic():
                user.update(first_name=form.cleaned_data['first_name'],
                            last_name=form.cleaned_data['last_name'],
                            email=form.cleaned_data['email'])

        return redirect('/setting')