from django.urls import path, re_path, include
from node_0.route.collection import AddCollections, ShowCollections, ReadCollections, DownloadCollection
from node_0.route.settings import Setting
from node_0.route.shutdown import Power
from node_0.route.table import Tables
from node_0.route.form import OrderForm, ShopForm, UpdateForm
from node_0.route.home import Dash

urlpatterns = [
    # Index
    path('', Dash.as_view(), name='dashboard'),
    # Purchase
    path('purchase', OrderForm.as_view(), name='purchase'),
    path('purchase/update/', UpdateForm.as_view(), name='purchase-update'),
    # Table
    re_path(r'^table$', Tables.as_view(), name='table'),
    # Setting
    path('setting', Setting.as_view(), name='setting'),
    # Component
    path('shop', ShopForm.as_view(), name='shop'),
    path('collections-add', AddCollections.as_view()),
    path('collections-show', ShowCollections.as_view(),
         name='show-collection'),
    path('collections', ReadCollections.as_view(), name='read-collection'),
    path('downloads', DownloadCollection.as_view(),
         name='download-collection'),

    # Shutdown
    path('accounts/', include('django.contrib.auth.urls')),

    # Disable url
    # path('shutdown', Power(), name='shutdown'),
    # path('change-password', views.home.change_password,name='change_password'),
    # path('things', views.collections.things, name='things'),
    # path('delete/toko/', Deletes.delete_toko, name='delete-toko'),
    # path('delete-order/', Deletes.delete_order, name='delete-order'),
]