from node_0.models import Order
import django_filters

class TableFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')
    shop = django_filters.CharFilter(field_name='shop__name', lookup_expr='exact')
    status = django_filters.CharFilter(field_name='status__code', lookup_expr='exact')

class Earn(django_filters.FilterSet):
    create_at = django_filters.DateTimeFilter('created_at', 'icontains')
