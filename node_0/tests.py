from django.test import TestCase
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from node_0.models import Toko
from django.test import RequestFactory, Client
import json


class Runner(TestCase):
    def setUp(self):
        self.client = Client()
        self.users = User.objects.create_superuser(username='test',
                                                   password='dev')

    def test_auth(self):
        user = authenticate(username='test', password='dev')

        self.assertIsNotNone(user)

    def test_shop(self):
        self.shop = Toko(username=self.users, name='test')
        self.shop.save()
        self.assertTrue(self.shop)

        _fname = Toko.objects.get(username=self.users, name='test')
        self.assertIsNotNone(_fname)

        Toko.objects.filter(pk=1).update(name='test 1')
        _ushop = Toko.objects.get(username=self.users)
        self.assertIsNotNone(_ushop)

    def tearDown(self):
        self.client.logout()


class ViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.users = User.objects.create_superuser(username='test',
                                                   password='dev')

    def test_auth_view(self):
        data = {'username': 'test', 'password': 'dev'}
        try_login = self.client.get('/')
        if try_login.status_code == 302:
            t1 = self.client.post(try_login.url, data=data)
            self.assertEqual(t1.status_code, 302)

        test_shop = self.client.get('/shop')

        self.assertEqual(test_shop.status_code, 200)

    def tearDown(self):
        self.client.logout()